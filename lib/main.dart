import 'package:flutter/material.dart';
import 'ui/pages/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Art Recall',
        theme: ThemeData(
          primaryColor: Colors.greenAccent,
        ),
        home: LoginPage(),
      );
}
