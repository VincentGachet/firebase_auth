import 'package:flutter/material.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton();

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context)!.canPop) {
      return GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Padding(
              padding: const EdgeInsets.all(10), child: Image.asset('assets/images/back.png', width: 36, height: 36)));
    } else {
      return Container();
    }
  }
}
