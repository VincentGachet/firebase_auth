import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'custom_back_button.dart';

class PageContainer extends StatelessWidget {
  const PageContainer({this.title, this.body, this.backgroundPath, this.backgroundColor});

  final String? title;
  final Widget? body;
  final String? backgroundPath;
  final Color? backgroundColor;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<String?>('title', title))
      ..add(DiagnosticsProperty<Widget?>('body', body))
      ..add(DiagnosticsProperty<String?>('backgroundPath', backgroundPath))
      ..add(DiagnosticsProperty<Color?>('backgroundColor', backgroundColor));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      backgroundColor: backgroundColor ?? Colors.white,
      body: SafeArea(
          child: Stack(children: <Widget>[
        backgroundPath != null
            ? Image.asset(backgroundPath!, height: MediaQuery.of(context).size.height, fit: BoxFit.cover)
            : Container(),
        Container(margin: ModalRoute.of(context)!.canPop ? const EdgeInsets.only(top: 46) : null, child: body),
        const CustomBackButton()
      ])));
}
