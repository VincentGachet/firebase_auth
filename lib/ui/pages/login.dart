import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
//import 'package:recall/ui/custom_views/dialog.dart';
import '../../managers/firebase_auth_manager.dart';
import '../custom_views/page_container.dart';
import 'phone_form.dart';
import 'profile.dart';
import 'register.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<FirebaseApp> _initializeFirebase() async {
    final FirebaseApp firebaseApp = await Firebase.initializeApp();

    final User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => ProfilePage(
            user: user,
          ),
        ),
      );
    }

    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) => PageContainer(
        backgroundPath: 'assets/images/fond.jpeg',
        body: FutureBuilder(
          future: _initializeFirebase(),
          builder: (pageContext, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        GestureDetector(
                            onTap: () async {
                              final User? user = await FireAuthManager.signInUsingApple();
                              if (user != null) {
                                print('connected Apple');
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(50)),
                              child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: const <Widget>[Icon(Icons.circle, size: 15), Text('  Sign in with Apple')]),
                            )),
                        GestureDetector(
                          onTap: () async {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => PhoneFormPage()),
                            );
                          },
                          child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(50)),
                              child: const Text('Sign in with phone')),
                        ),
                        GestureDetector(
                          onTap: () async {
                            print('launch email form');
                          },
                          child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(50)),
                              child: const Text('Sign in with email')),
                        ),
                        GestureDetector(
                          onTap: () async {
                            final User? user = await FireAuthManager.signInUsingGoogle(context: context);
                            if (user != null) {
                              print('connected Google');
                            }
                          },
                          child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration:
                                  BoxDecoration(color: Colors.redAccent, borderRadius: BorderRadius.circular(50)),
                              child: const Text('Sign in with Google', style: TextStyle(color: Colors.white))),
                        ),
                        GestureDetector(
                          onTap: () async {
                            final User? user = await FireAuthManager.signInUsingFacebook();
                            if (user != null) {
                              print("connected Facebook");
                            }
                          },
                          child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration:
                                  BoxDecoration(color: Colors.blueAccent, borderRadius: BorderRadius.circular(50)),
                              child: const Text('Sign in with Facebook', style: TextStyle(color: Colors.white))),
                        ),
                        Container(
                            margin: const EdgeInsets.only(top: 25),
                            child: const Text('Don\'t have an account yet ?', style: TextStyle(color: Colors.white))),
                        GestureDetector(
                          onTap: () async {
                            /*Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => RegisterPage(),
                          )
                        );*/
                          },
                          child: Container(
                              margin: const EdgeInsets.only(top: 15),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration:
                                  BoxDecoration(color: Colors.greenAccent, borderRadius: BorderRadius.circular(50)),
                              child: const Text('Sign up', style: TextStyle(color: Colors.white))),
                        )
                      ],
                    ),
                  ));
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      );
}
