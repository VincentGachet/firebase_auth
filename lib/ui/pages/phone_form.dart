import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:recall/managers/firebase_auth_manager.dart';
import 'package:recall/ui/custom_views/page_container.dart';
import 'package:recall/ui/pages/profile.dart';

class PhoneFormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PhoneFormPageState();
}

class _PhoneFormPageState extends State {
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _smsController = TextEditingController();
  String? _verificationId;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /*Future<void> showPhoneFormDialog(BuildContext context) async => showDialog(
        context: context,
        builder: (context) => StatefulBuilder(builder: (context, setState) => AlertDialog(
              content: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: _textEditingController,
                        validator: (value) => (value != null && value.isNotEmpty) ? null : 'Enter your phone number',
                        decoration:
                        const InputDecoration(hintText: 'Please enter your phone number'),
                      )
                    ],
                  )),
              actions: <Widget>[
                InkWell(
                  onTap: () {
                    if (_formKey.currentState != null && _formKey.currentState!.validate()) {
                      FireAuthManager.signInUsingPhone(_textEditingController.text);
                      Navigator.of(context).pop();
                    }
                  },
                  child: const Text('Send   '),
                ),
              ],
            )));*/

  @override
  Widget build(BuildContext context) => PageContainer(
      backgroundPath: 'assets/images/fond.jpeg',
      body: Container(
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Visibility(
                  visible: _verificationId == null,
                  child: TextFormField(
                    controller: _phoneNumberController,
                    validator: (value) => (value != null && value.isNotEmpty) ? null : 'Enter your phone number',
                    decoration: const InputDecoration(hintText: 'Please enter your phone number'),
                  )),
              Visibility(
                  visible: _verificationId != null,
                  child: TextFormField(
                    controller: _smsController,
                    validator: (value) => (value != null && value.isNotEmpty) ? null : 'Enter sms verification code',
                    decoration: const InputDecoration(hintText: 'Please enter sms verification code'),
                  )),
              ElevatedButton(
                  onPressed: () {
                    if (_verificationId == null) {
                      if (_phoneNumberController.text.isNotEmpty) {
                        _verifyPhoneNumber();
                      } else {
                        print('phone number field empty or null');
                      }
                    } else {
                      if (_smsController.text.isNotEmpty) {
                        _signInUsingPhoneNumber();
                      } else {
                        print('phone number field empty or null');
                      }
                    }
                  },
                  child: Text(_verificationId != null ? 'Log in' : 'Verify phone number'))
            ],
          )));


  //Tenter d'externaliser ces deux fonctions en les plaçant dans firebase_auth_manager avec un callback quand on reçoit le verificationId
  Future<void> _verifyPhoneNumber() async {
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: _phoneNumberController.text,
          timeout: const Duration(seconds: 15),
          verificationCompleted: (PhoneAuthCredential phoneAuthCredential) async {
            await _auth.signInWithCredential(phoneAuthCredential);
            print("Phone number automatically verified and user signed in: ${_auth.currentUser?.uid}");
          },
          verificationFailed: (FirebaseAuthException authException) {
            print('Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
          },
          codeSent: (String verificationId, int? resendToken) async {
            print('Please check your phone for the verification code.');
            _verificationId = verificationId;
            setState(() {});
          },
          codeAutoRetrievalTimeout: (String verificationId) {
            print('verification code: ' + verificationId);
            _verificationId = verificationId;
            setState(() {});
          });
    } catch (e) {
      print("Failed to Verify Phone Number: ${e}");
    }
  }

  Future<void> _signInUsingPhoneNumber() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId!,
        smsCode: _smsController.text,
      );

      final User? user = (await _auth.signInWithCredential(credential)).user;

      if (user != null) {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => ProfilePage(
              user: user,
            ),
          ),
        );
      } else {
        print('user is null');
      }
    } catch (e) {
      print('Failed to sign in: ' + e.toString());
    }
  }
}
