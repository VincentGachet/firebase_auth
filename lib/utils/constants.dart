class Constants {
  static const String formTypeText = 'formTypeText';
  static const String formTypeEmail = 'formTypeEmail';
  static const String formTypePassword = 'formTypePassword';
  static const String formTypeButton = 'formTypeButton';
}
