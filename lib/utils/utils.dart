import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:recall/utils/constants.dart';

class Utils {
  /// Generates a cryptographically secure random nonce, to be included in a
  /// credential request.
  String generateNonce([int length = 32]) {
    const String charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)]).join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<void> showFormDialog(BuildContext context, content, String title) async {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    final TextEditingController _textEditingController = TextEditingController();

    return await showDialog(
        context: context,
        builder: (context) {
          bool isChecked = false;
          return StatefulBuilder(
              builder: (context, setState) => AlertDialog(
                    content: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            /*TextFormField(
                        controller: _textEditingController,
                        validator: (value) {
                          return value.isNotEmpty ? null : "Enter any text";
                        },
                        decoration:
                        InputDecoration(hintText: "Please Enter Text"),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Choice Box"),
                          Checkbox(
                              value: isChecked,
                              onChanged: (checked) {
                                setState(() {
                                  isChecked = checked;
                                });
                              })
                        ],
                      )*/
                          ],
                        )),
                    title: Text(title),
                    actions: <Widget>[
                      InkWell(
                        child: Text('OK   '),
                        onTap: () {
                          /*if (_formKey.currentState.validate()) {
                      // Do something like updating SharedPreferences or User Settings etc.
                      Navigator.of(context).pop();
                    }*/
                        },
                      ),
                    ],
                  ));
        });
  }

  /*Widget getFormFields(String formType) {
    switch(formType) {
      case Constants.formTypeText:
        return TextFormField(
          controller: _textEditingController,
          validator: (value) {
            return value.isNotEmpty ? null : "Enter any text";
          },
          decoration:
          InputDecoration(hintText: "Please Enter Text"),
        )
    }
  }*/
}
